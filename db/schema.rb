# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_11_080318) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administrators", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_administrators_on_email", unique: true
    t.index ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true
  end

  create_table "adverts", force: :cascade do |t|
    t.integer "city_id"
    t.string "title"
    t.string "url"
    t.string "description"
    t.string "image"
    t.integer "total_views", default: 0, null: false
    t.integer "total_redirects", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "show_on_all_pages", default: false, null: false
    t.string "slug"
    t.index ["deleted"], name: "index_adverts_on_deleted"
    t.index ["description"], name: "index_adverts_on_description"
    t.index ["image"], name: "index_adverts_on_image"
    t.index ["title"], name: "index_adverts_on_title"
    t.index ["total_redirects"], name: "index_adverts_on_total_redirects"
    t.index ["total_views"], name: "index_adverts_on_total_views"
    t.index ["url"], name: "index_adverts_on_url"
  end

  create_table "cities", id: :integer, default: nil, force: :cascade do |t|
    t.integer "country_id", null: false
    t.boolean "important", null: false
    t.integer "region_id"
    t.string "title_ru", limit: 150
    t.string "avatar"
    t.string "iso_code"
    t.datetime "created_at", default: "2018-09-18 07:34:12", null: false
    t.datetime "updated_at", default: "2018-09-18 07:34:12", null: false
    t.text "weather"
    t.string "slug"
    t.string "meta_title"
    t.text "meta_description"
    t.string "meta_keywords"
    t.jsonb "adv_prices", default: {"day"=>50, "week"=>250, "month"=>900}, null: false
    t.index ["avatar"], name: "index_avatar2"
    t.index ["country_id"], name: "index_country_id1"
    t.index ["iso_code"], name: "index_iso_code2"
    t.index ["region_id"], name: "index_region_id"
    t.index ["title_ru"], name: "index_title_ru1"
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "descriptor"
    t.boolean "system", default: false, null: false
    t.string "meta_title"
    t.text "meta_description"
    t.text "meta_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.boolean "show_header", default: true, null: false
    t.index ["descriptor"], name: "index_pages_on_descriptor"
    t.index ["system"], name: "index_pages_on_system"
    t.index ["title"], name: "index_pages_on_title"
  end

  create_table "post_categories", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "avatar"
    t.string "large_avatar"
    t.string "slug"
    t.string "meta_title"
    t.string "meta_description"
    t.string "meta_keywords"
    t.index ["avatar"], name: "index_post_categories_on_avatar"
    t.index ["large_avatar"], name: "index_post_categories_on_large_avatar"
    t.index ["title"], name: "index_post_categories_on_title"
  end

  create_table "post_comments", force: :cascade do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.string "content"
    t.integer "positive_rating", default: 0
    t.integer "negative_rating", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "deleted", default: false, null: false
    t.index ["post_id"], name: "index_post_comments_on_post_id"
    t.index ["user_id"], name: "index_post_comments_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.integer "post_category_id"
    t.string "title", default: ""
    t.text "description", default: ""
    t.text "lead", default: ""
    t.string "avatar"
    t.string "large_avatar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "meta_title"
    t.string "meta_description"
    t.string "meta_keywords"
    t.integer "publisher_id"
    t.integer "state", default: 1, null: false
    t.integer "city_id", null: false
    t.integer "region_id", null: false
    t.integer "fake_modificator", default: 0, null: false
    t.integer "total_views", default: 0, null: false
    t.index ["avatar"], name: "index_posts_on_avatar"
    t.index ["large_avatar"], name: "index_posts_on_large_avatar"
    t.index ["post_category_id"], name: "index_posts_on_post_category_id"
    t.index ["title"], name: "index_posts_on_title"
  end

  create_table "publishers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.jsonb "permissions", default: {"index_posts"=>true, "create_posts"=>true, "update_posts"=>true, "destroy_posts"=>true, "update_profile"=>true}, null: false
    t.boolean "full_access", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "city_id", null: false
    t.integer "region_id", null: false
    t.string "company_name"
    t.string "old_email"
    t.boolean "deleted", default: false, null: false
    t.datetime "deleted_at"
    t.string "avatar"
    t.string "slug"
    t.index ["email"], name: "index_publishers_on_email", unique: true
    t.index ["full_access"], name: "index_publishers_on_full_access"
    t.index ["permissions"], name: "index_publishers_on_permissions"
    t.index ["reset_password_token"], name: "index_publishers_on_reset_password_token", unique: true
  end

  create_table "regions", id: :integer, default: nil, force: :cascade do |t|
    t.integer "country_id", null: false
    t.string "title_ru", limit: 150
    t.string "title_en", limit: 150
    t.text "description_ru"
    t.text "description_en"
    t.string "avatar"
    t.string "large_avatar"
    t.string "slug_ru"
    t.string "slug_en"
    t.string "meta_title_ru"
    t.string "meta_title_en"
    t.text "meta_description_ru"
    t.text "meta_description_en"
    t.string "meta_keywords_ru"
    t.string "meta_keywords_en"
    t.string "iso_code"
    t.datetime "created_at", default: "2018-09-18 07:34:09", null: false
    t.datetime "updated_at", default: "2018-09-18 07:34:09", null: false
    t.index ["avatar"], name: "index_avatar1"
    t.index ["country_id"], name: "index_country_id"
    t.index ["iso_code"], name: "index_iso_code1"
    t.index ["large_avatar"], name: "index_large_avatar1"
    t.index ["meta_description_en"], name: "index_meta_description_en1"
    t.index ["meta_description_ru"], name: "index_meta_description_ru1"
    t.index ["meta_keywords_en"], name: "index_meta_keywords_en1"
    t.index ["meta_keywords_ru"], name: "index_meta_keywords_ru1"
    t.index ["meta_title_en"], name: "index_meta_title_en1"
    t.index ["meta_title_ru"], name: "index_meta_title_ru1"
    t.index ["slug_en"], name: "index_slug_en1"
    t.index ["slug_ru"], name: "index_slug_ru1"
    t.index ["title_en"], name: "index_title_en1"
    t.index ["title_ru"], name: "index_title_ru"
  end

  create_table "statistic_records", force: :cascade do |t|
    t.date "date"
    t.integer "item_type", null: false
    t.integer "item_id", null: false
    t.integer "views", default: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], null: false, array: true
    t.index ["date"], name: "index_statistic_records_on_date"
    t.index ["item_id"], name: "index_statistic_records_on_item_id"
    t.index ["item_type"], name: "index_statistic_records_on_item_type"
    t.index ["views"], name: "index_statistic_records_on_views"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
