@region_id = City.find(14483).region_id
def create_publisher(id, company_name)
  user = Publisher.new(
      :email                 => "publisher#{id}@vestida.ru",
      :password              => "12345",
      :password_confirmation => "12345"
  )
  user.company_name = company_name
  user.city_id = 14483
  user.region_id = @region_id
  user.save!
  user.encrypted_password="$2a$10$LhFI1e6PxdnfPCUHwPFSJenkIIkntkd9v4SQd.BvCuC8VtuHdAqUi"
  File.open("public/demo/posts/new/#{rand(1..28)}.jpg") do |f|
    user.avatar = f
  end
  if user.save
    puts user.email
  else
    puts user.errors.to_a
  end
end

Publisher.destroy_all
['Воховские огни', 'Субботний вестник', 'Алексей Ивушкин', 'Naksha'].each_with_index do |name, i|
  create_publisher(i, name)
end

puts "Publishers added \n \n"
