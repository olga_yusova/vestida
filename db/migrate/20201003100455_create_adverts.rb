class CreateAdverts < ActiveRecord::Migration[5.2]
  def change
    create_table :adverts do |t|
      t.integer :city_id

      t.string :title, index: true
      t.string :url, index: true
      t.string :description, index: true
      t.string :image, index: true

      t.integer :total_views, index: true, null: false, default: 0
      t.integer :total_redirects, index: true, null: false, default: 0

      t.boolean :deleted, index: true, null: false , default: false
      t.timestamps
    end
  end
end
