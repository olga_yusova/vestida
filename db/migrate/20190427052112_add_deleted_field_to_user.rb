class AddDeletedFieldToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :publishers, :old_email, :string, index: true
    add_column :publishers, :deleted, :boolean, index: true, null: false, default: false
    add_column :publishers, :deleted_at, :datetime, index: true
  end
end
