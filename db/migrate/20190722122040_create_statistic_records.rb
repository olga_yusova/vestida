class CreateStatisticRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :statistic_records do |t|
      t.date :date, index: true
      t.integer :item_type, index: true, null: false
      t.integer :item_id, index: true, null: false
      t.integer :views, index: true, array: true, null: false, default: Array.new(24) {|i| 0 }
    end
  end
end
