class AddAllPagesLabelToAdvert < ActiveRecord::Migration[5.2]
  def change
    add_column :adverts, :show_on_all_pages, :boolean, index: true, null: false, default: false
    add_column :adverts, :slug, :string, index: true

    Advert.all.each do |adv|
      adv.update_attribute(:slug, Devise.friendly_token)
    end
  end
end
