class AddStateToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :state, :integer, index: true, null: false, default: 1
  end
end
