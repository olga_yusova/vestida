class AddSlugToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :slug, :string, index: true
    add_column :publishers, :slug, :string, index: true
  end
end
