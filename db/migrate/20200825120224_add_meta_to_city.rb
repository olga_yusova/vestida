class AddMetaToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :meta_title, :string, index: true
    add_column :cities, :meta_description, :text, index: true
    add_column :cities, :meta_keywords, :string, index: true
  end
end
