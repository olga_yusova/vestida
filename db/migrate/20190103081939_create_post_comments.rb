class CreatePostComments < ActiveRecord::Migration[5.2]
  def change
    create_table :post_comments do |t|
      t.integer :post_id, index: true
      t.integer :user_id, index: true
      t.string :content
      t.integer :positive_rating, default: 0
      t.integer :negative_rating, default: 0
      t.timestamps
    end
  end
end
