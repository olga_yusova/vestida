class CreateComments < ActiveRecord::Migration[5.2]
  def change
    add_column :post_comments, :deleted, :boolean, index: true, null: false, default: false
  end
end
