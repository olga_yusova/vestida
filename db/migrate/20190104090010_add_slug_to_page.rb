class AddSlugToPage < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :slug, :string, index: true
  end
end
