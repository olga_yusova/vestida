class AddGeoIdsToPost < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :city_id, :integer, index: true, null: false
    add_column :posts, :region_id, :integer, index: true, null: false
  end
end
