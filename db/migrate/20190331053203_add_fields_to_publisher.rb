class AddFieldsToPublisher < ActiveRecord::Migration[5.2]
  def change
    add_column :publishers, :city_id, :integer, index: true, null: false
    add_column :publishers, :region_id, :integer, index: true, null: false

    add_column :publishers, :company_name, :string, index: true
  end
end
