class AddMetaTags < ActiveRecord::Migration[5.2]
  def change

    add_column :post_categories, :slug, :string
    add_column :post_categories, :meta_title, :string
    add_column :post_categories, :meta_description, :string
    add_column :post_categories, :meta_keywords, :string

    add_column :posts, :slug, :string
    add_column :posts, :meta_title, :string
    add_column :posts, :meta_description, :string
    add_column :posts, :meta_keywords, :string
  end
end
