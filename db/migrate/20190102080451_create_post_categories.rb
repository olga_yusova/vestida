class CreatePostCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :post_categories do |t|
      t.string :title, index: true
      t.text :description
      t.string :avatar, index: true
      t.string :large_avatar, index: true
    end
  end
end
