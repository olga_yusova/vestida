class CreateMenus < ActiveRecord::Migration[5.2]
  def change
    create_table :menus do |t|
      t.string :title, index: true
      t.string :descriptor, index: true, unique: true
    end
    create_table :menu_items do |t|
      t.integer :menu_id, index: true
      t.string :title, index: true
      t.string :link, index: true
      t.integer :position, index: true, null: false, default: 0
    end
  end
end
