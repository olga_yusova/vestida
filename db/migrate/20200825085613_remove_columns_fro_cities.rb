class RemoveColumnsFroCities < ActiveRecord::Migration[5.2]
  def change
    remove_column :cities, :area_ru
    remove_column :cities, :region_ru
    remove_column :cities, :title_en
    remove_column :cities, :area_en
    remove_column :cities, :region_en
    remove_column :cities, :description_ru
    remove_column :cities, :description_en
    remove_column :cities, :large_avatar
    remove_column :cities, :slug_ru
    remove_column :cities, :slug_en
    remove_column :cities, :meta_title_ru
    remove_column :cities, :meta_title_en

    remove_column :cities, :meta_description_ru
    remove_column :cities, :meta_description_en

    remove_column :cities, :meta_keywords_ru
    remove_column :cities, :meta_keywords_en

    drop_table :menu_items
    drop_table :menus
  end
end
