class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|

      t.integer :post_category_id, index: true

      t.string :title, index: true
      t.text :description
      t.text :lead

      t.string :avatar, index: true
      t.string :large_avatar, index: true

      t.timestamps
    end
  end
end
