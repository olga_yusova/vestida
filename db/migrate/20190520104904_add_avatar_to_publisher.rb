class AddAvatarToPublisher < ActiveRecord::Migration[5.2]
  def change
    add_column :publishers, :avatar, :string, index: true
  end
end
