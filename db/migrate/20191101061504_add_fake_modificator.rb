class AddFakeModificator < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :fake_modificator, :integer, index: true, null: false, default: 0
    add_column :posts, :total_views, :integer, index: true, null: false, default: 0

    Post.all.each do |post|
      post.fake_modificator = rand(75..100)
      post.save
    end
  end
end
