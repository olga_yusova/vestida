class AddDefaultValueToPostLead < ActiveRecord::Migration[5.2]
  def change
    change_column :posts, :lead, :text, default: ''
  end
end
