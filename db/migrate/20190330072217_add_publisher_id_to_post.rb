class AddPublisherIdToPost < ActiveRecord::Migration[5.2]
  def change
    remove_column :posts, :user_id
    add_column :posts, :publisher_id, :integer, index: true
  end
end
