class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title, index: true
      t.text :description
      t.string :descriptor, index: true
      t.boolean :system, index: true, null: false, default: false

      t.string :meta_title
      t.text :meta_description
      t.text :meta_keywords

      t.timestamps
    end
  end
end
