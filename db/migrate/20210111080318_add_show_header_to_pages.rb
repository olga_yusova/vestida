class AddShowHeaderToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :show_header, :boolean, index: true, null: false, default: true
  end
end
