class AddWeatherWidgetToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :weather, :text, index: true
  end
end
