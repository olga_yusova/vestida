# README

Для запуска проекта локально нужно выполнить следующие команды

* git clone https://dima_antonenko@bitbucket.org/dima_antonenko/for_olga.git
* перейти в папку с проектом
* bundle install
* rake db:create
* rails s


# Приоритетные задачи

* Добавить декораторы
* Добавить фасады
* Добавить Pundit
* ~~Подключить devise~~
* Подключить и настроить capistrano
* Разделить routes.rb на несколько файлов
* Подключить вебпакер
* Подключить rspec + factory_bot + faker


psql -U postgres -d vestida_production -f vestida_production.sql
RAILS_ENV=production rbenv exec bundle exec rails c

Post.all.each {|x| puts x.avatar.recreate_versions! if x.avatar.to_s.size > 3}
Post.all.each {|x|x.large_avatar.recreate_versions! if x.large_avatar.to_s.size > 3}
Publisher.all.each {|x|x.avatar.recreate_versions! if x.avatar.to_s.size > 3}
