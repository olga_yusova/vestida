
#!/usr/bin/env puma

directory '/home/deploy/applications/vestida/current/'
rackup "/home/deploy/applications/vestida/current/config.ru"
environment 'production'

pidfile "/home/deploy/applications/vestida/shared/tmp/pids/puma.pid"
state_path "/home/deploy/applications/vestida/shared/tmp/pids/puma.state"
stdout_redirect '/home/deploy/applications/vestida/shared/log/puma_access.log', '/home/deploy/applications/vestida/shared/log/puma_error.log', true


threads 0,16

bind 'unix:///home/deploy/applications/vestida/shared/tmp/sockets/puma.sock'

workers 0

daemonize true

prune_bundler


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "/home/deploy/applications/vestida/current/Gemfile"
end