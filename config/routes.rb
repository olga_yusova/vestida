Rails.application.routes.draw do


  devise_for :publishers, controllers: { sessions: 'publishers/sessions' }
  devise_for :administrators
  devise_for :users, controllers: {
        registrations: 'users/registrations',
        sessions: 'users/sessions'
      }

  get 'administrators/dashboard', as: 'administrator_root'
  get 'users/dashboard',          as: 'user_root'
  get 'publishers/dashboard',     as: 'publisher_root'

  root 'site/pages#home'
  get 'sitemap.xml' => 'site/pages#sitemap', format: :xml, as: :sitemap
  get 'robots'      => 'site/pages#robots',  format: :text
  get 'robots.txt'  => 'site/pages#robots',  format: :text
  get 'feeds'       => 'site/pages#feeds', format: 'rss'
  get 'feeds_hot'   => 'site/pages#feeds_hot', format: 'rss'

  # пути для общедоступной части
  scope module: 'site' do
    resources :pages,  only: [:show]
    resources :posts,  only: [:show]
    resources :post_categories,  only: [:show]

    resources :regions, only: [:show]
    resources :cities, only: [:show] do
      get 'test', on: :member
      get 'feed', on: :member, format: 'rss'
    end
    resources :issuers, only: [:show]
    resources :adverts, only: [:show] do
      get 'info', on: :member
    end
    resources :comments, only: [:create, :destroy]
  end

  # пути для админки
  namespace :administrators do
    resources :pages
    resources :posts
    resources :post_categories
    resources :publishers
    resources :cities

    resources :adverts, only: [:new, :create, :edit, :update, :destroy, :index] do
      post 'restore', on: :member
    end
  end

  namespace :publishers do
    resources :posts, only: [:new, :create, :edit, :update, :destroy, :index] do
      post 'publish', on: :member
      post 'unpublish', on: :member
    end
  end

  # редактирование профиля
  match "/publishers/edit_profile" => "publishers/profile#edit_profile", via: [:get]

  # обновление пароля
  match "/publishers/update_password" => "publishers/profile#update_password",
        via: [:post], as: 'update_password_publishers'

  # обновление профиля
  match "/publishers/update_profile" => "publishers/profile#update_profile",
        via: [:post], as: 'update_profile_publishers'


  # получение городов у региона
  match "/api/region_cities" => "site/api#region_cities", via: [:post]

  # получение городов у региона
  match "/api/set_city_id" => "site/api#set_city_id", via: [:post]

  get '/cities/:id/post_categories/:post_category_id',
      to: 'site/cities#post_category',
      as: 'city_post_category'

  get '/search', to: 'site/search#show'
end
