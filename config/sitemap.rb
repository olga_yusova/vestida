# Change this to your host. See the readme at https://github.com/lassebunk/dynamic_sitemaps
# for examples of multiple hosts and folders.
host "vestida.ru"

sitemap :site do
  url root_url, last_mod: Time.now, change_freq: "daily", priority: 1.0
  url "https://#{host}/pages/about-project"

  Post.all.each do |post|
    url "https://#{host}#{post_path(post)}"
  end

  City.all.each do |city|
    url "https://#{host}#{city_path(city)}"
  end

  # City.all.each do |city|
  #   PostCategory.all.each do |post_category|
  #     url "http://#{host}#{city_post_category_path(city, post_category)}"
  #   end
  # end
end

ping_with "https://#{host}/sitemap.xml"
