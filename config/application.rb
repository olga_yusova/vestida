require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ForOlga
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    # config.assets.paths << Rails.root.join("app", "assets", "fonts")

    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')
    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/

    # config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
    config.encoding = "utf-8"
    config.assets.enabled = false
    config.i18n.default_locale = :ru

    config.time_zone = 'St. Petersburg'
    config.active_record.default_timezone = :local

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.


    config.autoload_paths += Dir[
        "#{config.root}/lib/**/",
        "#{config.root}/app/services",
        "#{config.root}/app/services/**/",
        "#{config.root}/app/facades/**/",
        "#{config.root}/app/exceptions",
        "#{config.root}/app/policies/**/"
    ]


    config.generators do |g|
      g.assets false
      g.test_framework :rspec, fixtures: true, views: false
    end
    config.serve_static_assets = false
  end
end
