//= require rails-ujs
//= require jquery
//= require tinymce

//= require administrator/template/jquery.min
//= require administrator/template/bootstrap.bundle.min
//= require administrator/template/jquery.easing.min
//= require administrator/template/jquery.dataTables
//= require administrator/template/dataTables.bootstrap4
//= require administrator/template/admin
//= require administrator/template/selectize
//= require administrator/template/ion.rangeSlider

//= require_tree ./administrator/pages

(function($) {
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    });
})(jQuery);