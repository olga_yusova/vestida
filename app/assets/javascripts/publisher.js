//= require rails-ujs
//= require jquery
//= require tinymce

//= require publisher/template/jquery.min
//= require publisher/template/bootstrap.bundle.min
//= require publisher/template/jquery.easing.min
//= require publisher/template/jquery.dataTables
//= require publisher/template/dataTables.bootstrap4
//= require publisher/template/admin
//= require publisher/template/selectize
//= require publisher/template/ion.rangeSlider

//= require_tree ./publisher/pages

(function($) {
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    });
})(jQuery);
