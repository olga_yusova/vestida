$('#main_select_region').selectize({});
$('#main_select_city').selectize({});

if (document.body.className.match('adverts-new') ) {

$("#main_select_region").change(function () {
    state_select_city($(this).val());
});


function state_select_city(region_id) {
    $.ajax({
        type: 'POST',
        url: '/api/region_cities',
        data: {
            'region_id': region_id,
        },
        success: function (msg) {
            console.log(msg);
            clear_city_area();
            for (let city of msg) {
                //console.log(city_id);
                $('#main_select_city').append('<option value="' + city.id + '">' + city.title_ru + '</option>');
            }
            $('#main_select_city').selectize({});
        }

    });

    function clear_city_area() {
        $('#select_city_area').empty();
        $('#select_city_area').append('<label>Город</label>');
        $('#select_city_area').append('<select id="main_select_city" name="advert[city_id]"></select>');
        $('#main_select_city').append('<option value="0">Все города</option>');
    }

}

}
