if (document.body.className.match('publishers-index') ) {

    $('#main_select_region').selectize({});
    $('#main_select_city').selectize({});


    $("#main_select_region").change(function () {
        state_select_city($(this).val());
    });

    function state_select_city(region_id) {
        $.ajax({
            type: 'POST',
            url: '/api/region_cities',
            data: {
                'region_id': region_id,
            },
            success: function (msg) { // если запрос успешен - рендерим селект с городами
                console.log(msg);
                clear_city_area();
                for (let city of msg) {
                    $('#main_select_city').append('<option value="' + city.id + '">' + city.title_ru + '</option>');
                }
                $('#main_select_city').selectize({});

            }

        });

        function clear_city_area() {
            $('#select_city_area').empty();
            $('#select_city_area').append('<label>Город</label>');
            $('#select_city_area').append('<select id="main_select_city" name="publisher[city_id]"></select>');
            $('#main_select_city').append('<option value="0">Все города</option>');
        }

    }


// data-type-resource data-id-resource
    function generate_link(region_id, city_id) {
        $('#link_area').empty();
        if (city_id == 0) {
            //  href = '/regions/' + region_id
        } else {
            href = '/cities/' + city_id
        }
        $('#link_area').append('<a href="' + href + '">Перейти</a>');
    }

}
