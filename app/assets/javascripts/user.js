//= require rails-ujs
//= require jquery
//= require tinymce

//= require user/template/jquery.min
//= require user/template/bootstrap.bundle.min
//= require user/template/jquery.easing.min
//= require user/template/jquery.dataTables
//= require user/template/dataTables.bootstrap4
//= require user/template/admin
//= require user/template/selectize
//= require user/template/ion.rangeSlider

//= require_tree ./user/pages

(function($) {
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    });
})(jQuery);
