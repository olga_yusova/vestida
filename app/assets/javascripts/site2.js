//= require rails-ujs
//= require site2/template/vendor/modernizr-3.5.0.min.js
//= require site2/template/vendor/jquery-1.12.4.min.js
//= require site2/template/vendor/popper.min.js
//= require site2/template/vendor/bootstrap.min.js
//= require site2/template/vendor/jquery.slicknav.js
//= require site2/template/vendor/owl.carousel.min.js
//= require site2/template/vendor/slick.min.js
//= require site2/template/vendor/wow.min.js
//= require site2/template/vendor/animated.headline.js
//= require site2/template/vendor//jquery.magnific-popup.js
//= require site2/template/vendor/jquery.ticker.js
//= require site2/template/vendor/jquery.scrollUp.min.js
//= require site2/template/vendor/jquery.sticky.js
//= require site2/template/vendor/perfect-scrollbar.js
//= require site2/template/vendor/waypoints.min.js
//= require site2/template/vendor/jquery.counterup.min.js
//= require site2/template/vendor/jquery.theia.sticky.js

//= require site2/template/main.js

//= require selectize

//= require site/template/selects-in-head


// код на всех страницах
(function($) {
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    });
})(jQuery);
