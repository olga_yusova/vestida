var city_id = $("body").data('city-id');
var region_id = $("body").data('region-id');
var city_title = $("body").data('city-title');

console.log(region_id, city_id, city_title);


if (region_id != 0) {
     $('#main_select_region').selectize({
         onInitialize: function() {
             this.setValue(region_id);
         }
     });

} else {
    $('#main_select_region').selectize({});
}


$( document ).ready(function() {
    if (city_id != 0) {
        $('#select_city_area').empty();
        $('#select_city_area').append('<select id="main_select_city"></select>');

         $.ajax({
            type: 'POST',
            url: '/api/region_cities',
            async: false,
            data: {
                'region_id': region_id,
            },
            success: function(msg) {
                $('#main_select_city').selectize({
                    valueField: 'id',
                    labelField: 'title_ru',
                    create: true,
                    options: msg,

                    onInitialize: function () {
                        this.setValue(city_id);
                    }
                });
            }

        });
    } else {
        $('#main_select_city').selectize({});
    }
});


//написать установку начальных значений

$("#main_select_region").change(function() {
    state_select_city($(this).val());
    $('#link_area').empty();
    // generate_link($("#main_select_region").val(),
    //     $("#main_select_city").val());
});


function state_select_city(region_id) {
    $.ajax({
        type: 'POST',
        url: '/api/region_cities',
        data: {
            'region_id': region_id,
        },
        success: function(msg) { // если запрос успешен - рендерим селект с городами
            console.log(msg);
            clear_city_area();
            for (let city of msg) {
                $('#main_select_city').append('<option value="' + city.id + '">' + city.title_ru + '</option>');
            }
            $('#main_select_city').selectize({
                onChange: function() {
                    generate_link($("#main_select_region").val(),
                        $("#main_select_city").val());
                }
            });


        }

    });


}


function get_cites_from_region(region_id) {


}


function clear_city_area() {
    $('#select_city_area').empty();
    $('#select_city_area').append('<select id="main_select_city"></select>');
    $('#main_select_city').append('<option value="0">Все города</option>');
}


// data-type-resource data-id-resource
function generate_link(region_id, city_id) {
    $('#link_area').empty();
    if (city_id == 0) {
        //  href = '/regions/' + region_id
    } else {
        href = '/cities/' + city_id
    }
    $('#link_area').append('<a href="#" id="link-button" data-city-id="'+ city_id +'">Перейти</a>');
}



$(document).on("click", "#link_area a", function() {
    var city_id = $("#main_select_city").val();


    $.ajax({
        type: 'POST',
        url: '/api/set_city_id',
        data: {
            'city_id': city_id,
        },
        success: function(msg) {
            console.log(msg);
            location.reload();
        }

    });
});