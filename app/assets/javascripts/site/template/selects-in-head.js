$('.main_select_region').selectize({});
$('.main_select_city').selectize({});


$(".main_select_region").change(function() {
    state_select_city($(this).val());
    generate_link($(".main_select_city").val());
});


function state_select_city(region_id) {
    $.ajax({
        type: 'POST',
        url: '/api/region_cities',
        data: {
            'region_id': region_id,
        },
        success: function(msg) { // если запрос успешен - рендерим селект с городами
          //  console.log(msg);
            clear_city_area();
            for (let city of msg) {
                $('.main_select_city').append('<option value="' + city.url + '">' + city.title_ru + '</option>');
            }
            $('.main_select_city').selectize({
                onChange: function(value) {
                    generate_link(value);
                }
            });


        }

    });

    function clear_city_area() {
        $('.select_city_area').empty();
        $('.select_city_area').append('<select class="main_select_city"></select>');
        $('.main_select_city').append('<option value="0">Все города</option>');
    }

}





// data-type-resource data-id-resource
function generate_link(city_url) {
    $('.link_area').empty();
    if (city_url == 0) {
      //  href = '/regions/' + region_id
    } else {
        $('.link_area').append('<a href="' + city_url + '">Перейти</a>');
    }

}

