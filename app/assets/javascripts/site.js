//= require rails-ujs
//= require site/template/jquery-3.2.1.min
//= require site/template/zebra_tooltips.min
//= require site/template/owl.carousel.min.js
//= require site/template/main-script
//= require selectize

//= require site/template/selects-in-head

//кастомизация страниц
//= require_tree ./site/pages

// код на всех страницах
(function($) {
    $(document).ajaxSend(function(e, xhr, options) {
        var token = $('meta[name="csrf-token"]').attr('content');
        if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    });
})(jQuery);



