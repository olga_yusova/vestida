class ApplicationPolicy

  def access_to_component?(user, permission)
    if user && (user.permissions[permission] == true || user.full_access)
      true
    else
      fail(AccessDenied.new)
    end
  end
end