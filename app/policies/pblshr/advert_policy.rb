module Pblshr
  class AdvertPolicy < ApplicationPolicy

    attr_accessor(:publisher, :advert)

    def initialize(publisher, advert)
      @publisher = publisher
      @advert = advert
    end

    def index?
      publisher
    end

    def create?
      publisher
    end

    def update?
      publisher && access_to_advert?
    end

    def destroy?
      publisher && access_to_advert?
    end

    private

    def access_to_advert?
      advert.publisher_id == publisher.id || publisher.full_access
    end
  end
end
