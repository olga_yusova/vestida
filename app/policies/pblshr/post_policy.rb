module Pblshr
  class PostPolicy < ApplicationPolicy

    attr_accessor(:publisher, :post)

    def initialize(publisher, post)
      @publisher = publisher
      @post = post
    end

    def index?
      access_to_component?(publisher, 'index_posts') ? true : fail(AccessDenied.new)
    end

    def create?
      access_to_component?(publisher, 'create_posts') ? true : fail(AccessDenied.new)
    end

    def update?
      access_to_post? && access_to_component?(publisher, 'update_posts') ? true : fail(AccessDenied.new)
    end

    def destroy?
      access_to_post? && access_to_component?(publisher, 'destroy_posts') ? true : fail(AccessDenied.new)
    end

    private

    def access_to_post?
      post.publisher_id == publisher.id || publisher.full_access
    end
  end
end