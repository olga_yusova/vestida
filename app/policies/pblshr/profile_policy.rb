module Pblshr
  class ProfilePolicy < ApplicationPolicy

    attr_accessor(:publisher)

    def initialize(publisher)
      @publisher = publisher
    end

    def access_to_profile?
      access_to_component?(publisher, 'profile') ? true : raise(AccessDenied.new)
    end
  end
end