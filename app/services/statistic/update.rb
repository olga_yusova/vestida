module Statistic
  class Update

    attr_accessor :item_type, :item_id, :hour

    def initialize(item_type, item_id)
      @item_type, @item_id = item_type, item_id
      @hour = Time.new.hour
    end

    def call
      statistic_record = get_statistic_record
      puts "<<<<<<<<<<<<<< #{statistic_record.views[hour]}"
      statistic_record.views[hour] += 1
      statistic_record.save
      puts "<<<<<<<<<<<<<< #{statistic_record.views[hour]}"
    end

    private

    def get_statistic_record
      scope = StatisticRecord.where(date: DateTime.now.to_date, item_type: item_type, item_id: item_id)
      if scope.size > 0
        scope.first
      elsif scope.size == 0
        StatisticRecord.create(date: DateTime.now.to_date, item_type: item_type, item_id: item_id)
      end
    end
  end
end