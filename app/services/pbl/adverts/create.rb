module Pbl
  module Adverts
    class Create

      attr_accessor :publisher, :params, :advert

      def initialize(publisher, params)
        @publisher, @params = publisher, params
        @advert = Advert.new
      end

      def call
        assign!
        advert
      end

      protected

      def assign!
        advert.assign_attributes(params)
        advert.published_to = (DateTime.now + 1.month).to_date
      end
    end
  end
end
