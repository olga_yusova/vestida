module UiHelper
  def pretty_datetime(datetime)
    datetime.to_s.chomp('UTC').chomp(' +0300')
  end

  def pretty_price(price)
    number_to_currency(price, precision: 0, delimiter: '', format: "%n %u", locale: :ru, :unit => 'р.')
  end

  def get_post_lead(post)
    lead = post.lead
    if lead && lead.size > 220
      lead[0...220] + " <a href=\"/posts/#{post.id}/\">...</a>"
    else
      lead.to_s
    end
  end

  def string_state_of_post(post)
    if post.not_published?
      'Не опубликована'
    elsif post.published?
      'Опубликована'
    elsif post.deleted?
      'Удалена'
    end
  end

  def get_string_hours
    ['00:00 - 01:00',
     '01:00 - 02:00',
     '02:00 - 03:00',
     '03:00 - 04:00',
     '04:00 - 05:00',
     '05:00 - 06:00',
     '06:00 - 07:00',
     '07:00 - 08:00',
     '08:00 - 09:00',
     '09:00 - 10:00',
     '10:00 - 11:00',
     '11:00 - 12:00',
     '12:00 - 13:00',
     '13:00 - 14:00',
     '14:00 - 15:00',
     '15:00 - 16:00',
     '16:00 - 17:00',
     '17:00 - 18:00',
     '18:00 - 19:00',
     '19:00 - 20:00',
     '20:00 - 21:00',
     '21:00 - 22:00',
     '22:00 - 23:00',
     '23:00 - 00:00']
  end

  def strip_emoji(str)
    str = str.force_encoding('utf-8').encode
    clean_text = ""

    # emoticons  1F601 - 1F64F
    regex = /[\u{1f600}-\u{1f64f}]/
    clean_text = str.gsub regex, ''

    #dingbats 2702 - 27B0
    regex = /[\u{2702}-\u{27b0}]/
    clean_text = clean_text.gsub regex, ''

    # transport/map symbols
    regex = /[\u{1f680}-\u{1f6ff}]/
    clean_text = clean_text.gsub regex, ''

    # enclosed chars  24C2 - 1F251
    regex = /[\u{24C2}-\u{1F251}]/
    clean_text = clean_text.gsub regex, ''

    # symbols & pics
    regex = /[\u{1f300}-\u{1f5ff}]/
    clean_text = clean_text.gsub regex, ''
    clean_text = Sanitize.fragment(clean_text, :elements => ['p', 'b', 'a'])
    clean_text
  end

  def get_turbo_content(post)
    header = "<![CDATA[<header><h2>#{post.publisher.company_name}</h2>"
    img = post.avatar.to_s.size > 3 ? "<figure><img src='https://vestida.ru#{post.avatar.to_s }'></figure>" : ''
    desc = "</header>#{strip_emoji(post.description)} ]]>"

    res = header + img + desc
    res
  end
end
