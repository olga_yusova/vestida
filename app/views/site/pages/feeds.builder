xml.instruct!
xml.rss version: '2.0', 'xmlns:turbo' => 'http://www.w3.org/2005/Atom' do

  xml.channel do
    xml.title @settings[:title]
    xml.description @settings[:description]
    xml.link @settings[:link]
    xml.language 'ru'
    # xml.content namespace: 'turbo'

    for post in @posts
      xml.item turbo: true do
        xml.title post.title
        xml.link post_url(post)
        xml.tag!("pubDate", post.created_at.to_s)
        xml.tag!("turbo:content", get_turbo_content(post))
      end
    end

  end
end
