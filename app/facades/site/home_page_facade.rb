class Site::HomePageFacade

  require 'sypex_geo'
  attr_reader :page, :current_ip
  POSTS_QTY = 10

  def initialize(page, current_ip)
    @page = page
    @current_ip = current_ip
  end

  def base_object
    page
  end

  def location
    db = SypexGeo::Database.new('public/data/SxGeoCity.dat')
    res = db.query(current_ip)
    if res.city && res.city[:name_ru]
      city = get_city(res.city[:name_ru])
      city = City.find_by(title_ru: 'Москва') unless city
      city
    else
      City.find_by(title_ru: 'Москва')
    end
  end

  def sidebar_publishers
    Publisher.all.limit(10).order('RANDOM()').collect{|p| [p.id, p.company_name]}
  end

  def top_posts
    Post.all.order(:total_views).limit(5)
  end

  def publishers
    if location && location.id
      city_publihsers = Publisher.where(city_id: location.id).limit(5).includes(:posts)
      if city_publihsers.size > 0
        city_publihsers
      else
        random_publishers
      end
    else
      random_publishers
    end
  end

  private

  def random_publishers
    Publisher.all.limit(5).includes(:posts)
  end

  def get_city(title_ru)
    City.find_by(title_ru: res.city[:name_ru]) rescue nil
  end
end
