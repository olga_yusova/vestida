class Site::PublisherFacade

  attr_reader :publisher

  def initialize(publisher)
    @publisher = publisher
  end

  def base_object
    publisher
  end

  def city_title
    publisher.city.title_ru
  end

  def city_weather
    publisher.city.weather
  end

  def sidebar_publishers
    Publisher.where(city_id: publisher.city_id).limit(10).collect{|p| [p.id, p.company_name]}
  end

  def sidebar_categories
    PostCategory.select(:id, :title).all.collect do |c|
      {city_id: publisher.city_id, category_id: c.id, category_title: c.title}
    end
  end

  def top_posts
    Post.where(publisher_id: publisher.id).order(:total_views).limit(5)
  end

  def advert
    Advert.get(publisher.city_id)
  end
end
