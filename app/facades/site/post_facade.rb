class Site::PostFacade

  attr_reader :post

  def initialize(post)
    @post = post
  end

  def base_object
    post
  end

  def publisher
    post.publisher
  end

  def city
    post.city
  end

  def new_comment
    PostComment.new
  end

  def related
    Post.where.not(id: post.id).order('created_at DESC').limit(3)
  end

  def hot
    Post.where(city_id: post.city_id).where.not(id: post.id).order('RANDOM()').limit(3)
  end

  def new
    Post.where(city_id: post.city_id).where.not(id: post.id).order('created_at ASC').limit(3)
  end

  def viewed
    Post.where(city_id: post.city_id).where.not(id: post.id).order('total_views ASC').limit(3)
  end

  def city_comments
    PostComment.all.order('RANDOM()').limit(2).includes(:post)
  end

  def post_category
    post.post_category
  end

  def now_read
    Post.where.not(id: post.id).order('RANDOM()').limit(3)
  end

  def advert
    Advert.get(post.city_id)
  end
end
