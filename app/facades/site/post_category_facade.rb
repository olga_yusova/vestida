class Site::PostCategoryFacade

  attr_reader :post_category, :city

  def initialize(post_category, city)
    @post_category = post_category
    @city = city
  end

  def base_object
    post_category
  end

  def city_weather
    city.weather
  end

  def posts_size
    Post.where(city_id: city.id, post_category_id: post_category.id).size
  end

  def sidebar_categories
    PostCategory.select(:id, :title).all.collect do |c|
      {city_id: city.id, category_id: c.id, category_title: c.title}
    end
  end

  def top_posts
    Post.where(city_id: city.id).order(:total_views).limit(10)
  end

end
