class Site::CityFacade

  attr_reader :city

  def initialize(city)
    @city = city
  end

  def base_object
    city
  end

  def city_weather
    city.weather
  end

  def posts_size
    posts.size
  end

  def sidebar_publishers
    Publisher.where(city_id: city.id).limit(10).collect{|p| [p.id, p.company_name]}
  end

  def sidebar_categories
    PostCategory.select(:id, :title).all.collect do |c|
      {city_id: city.id, category_id: c.id, category_title: c.title}
    end
  end

  def top_posts
    Post.where(city_id: city.id).order(:total_views).limit(5)
  end

  def publishers
    Publisher.where(city_id: city.id).includes(:posts)
  end

  def meta_title
    if city.meta_title
      city.meta_title
    else
      "Новости города #{city.title_ru}"
    end
  end

  def meta_description
    if city.meta_description
      city.meta_description
    else
      "Официальное сетевое издание. На этой странице представлены самые значимые, актуальные и важные новости города #{city.title_ru}"
    end
  end

  def meta_keywords
    if city.meta_keywords
      city.meta_keywords
    else
      "Новости, рубрика Происшествия, новости города #{city.title_ru}, новости #{city.region.title_ru} , произошёл, обвиняет, случиться, Кражи, убийство, Злоумышленники, возгорание, Неизвестно, столкнулись, травмы, факт, потерпевших, инцидент, сбила, пропавших, очевидцев, смерть, возбуждено, следователя, рождения"
    end
  end

  def advert
    Advert.get(city.id)
  end
end
