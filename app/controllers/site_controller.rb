class SiteController < ApplicationController
  layout 'site2'
  before_action :all_pages_data

  private

  def all_pages_data
    # db = SypexGeo::Database.new('public/data/SxGeoCity.dat')
    @regions = Region.all.select(:id, :title_ru).order(:title_ru)
  end
end
