class Administrators::CitiesController < AdministratorsController

  before_action :set_city, only: [:edit, :update, :destroy]

  def index
    city_ids = Publisher.all.pluck(:city_id).uniq
    @cities = City.where(id: city_ids)
  end

  def edit;end

  def update
    @city.update_attributes(city_params)
    redirect_back(fallback_location: administrator_root_path)
  end
  private

  def set_city
    @city = City.friendly.find(params[:id])
  end

  def city_params
    params.require(:city).permit(:title_ru, :weather, :slug, :meta_title, :meta_description, :meta_keywords )
  end
end
