class Administrators::PagesController < AdministratorsController

  before_action :set_page, only: [:edit, :update, :destroy]

  def index
    @pages = Page.all
  end

  def new
    @page = Page.new()
  end

  def edit;end

  def create
    page = Page.new
    page.assign_attributes(page_params)
    if page.save
      redirect_to edit_administrators_page_path(page), notice: 'Запись успешно добавлена'
    else
      redirect_back(fallback_location: administrator_root_path)
    end
  end

  def update
    @page.update_attributes(page_params)
    redirect_back(fallback_location: administrator_root_path)
  end

  def destroy
    @page.destroy
    redirect_back(fallback_location: administrator_root_path)
  end

  private

  def set_page
    @page = Page.friendly.find(params[:id])
  end

  def page_params
    params.require(:page).permit(:title, :description, :slug, :meta_title, :meta_description,
                                 :meta_keywords, :show_header )
  end
end
