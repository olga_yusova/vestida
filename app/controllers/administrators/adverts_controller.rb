class Administrators::AdvertsController < AdministratorsController

  before_action :get_advert, only: [:edit, :update, :destroy, :restore]

  def index
    @adverts = Advert.all.includes(:city)
  end

  def edit;
  end

  def update
    @advert.update_attributes(advert_params)
    redirect_back(fallback_location: administrator_root_path)
  end

  def destroy
    @advert.update_column(:deleted, true)
    redirect_back(fallback_location: administrator_root_path)
  end

  def restore
    @advert.update_column(:deleted, false)
    redirect_back(fallback_location: administrator_root_path)
  end

  def new
    @advert = Advert.new
    @regions = Region.all.select(:id, :title_ru)
  end

  def create
    advert = Advert.new
    advert.assign_attributes(advert_params)
    advert.slug = Devise.friendly_token
    if advert.save
      redirect_to edit_administrators_advert_path(advert)
    else
      redirect_back(fallback_location: administrator_root_path)
    end
  end

  def show

  end

  private

  def get_advert
    @advert = Advert.friendly.find(params[:id])
  end

  def advert_params
    params.require(:advert).permit(:title, :description, :image, :city_id, :url,
                                   :show_on_all_pages)
  end
end
