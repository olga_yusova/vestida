class Administrators::PublishersController < AdministratorsController

  before_action :get_user, only: [:edit, :destroy, :update]

  def index
    @publishers = Publisher.where(deleted: false)
    @deleted_publishers = Publisher.deleted_all.includes(:posts)
    @new_publisher = Publisher.new
    @regions = Region.all.select(:id, :title_ru)
  end

  def edit
  end

  def destroy
    @publisher.old_email = @publisher.email
    @publisher.email = "deleted#{rand(0..100)}-#{@publisher.email}"
    @publisher.deleted = true
    @publisher.deleted_at = DateTime.now
    @publisher.save
    @publisher.posts.update_all(state: 0)
    redirect_back(fallback_location: administrator_root_path)
  end

  def create
    email = params[:publisher][:email]
    if Publisher.where(email: email).size == 0
      create_publisher(email, params[:publisher][:city_id], params[:publisher][:company_name])
    end
    redirect_back(fallback_location: administrator_root_path)
  end

  def show
    publisher = Publisher.find(params[:id])
    bypass_sign_in(publisher)
    redirect_to(publisher_root_url)
  end

  def update
    @publisher.update_attribute(:company_name, params[:publisher][:company_name])
    redirect_back(fallback_location: administrator_root_path)
  end

  private

  def create_publisher(email, city_id, company_name)
    city = City.find(city_id)
    user = Publisher.new(email: email, password: "12345", password_confirmation: "12345",
                         city_id: city.id, region_id: city.region_id, company_name: company_name)
    user.save!
    user.encrypted_password = "$2a$10$LhFI1e6PxdnfPCUHwPFSJenkIIkntkd9v4SQd.BvCuC8VtuHdAqUi"
    user.save
  end

  def get_user
    @publisher = Publisher.find(params[:id])
  end
end