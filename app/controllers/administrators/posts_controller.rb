class Administrators::PostsController < AdministratorsController

  before_action :get_post, only: [:edit, :update, :destroy]
  before_action :get_post_categories, only: [:edit, :new]

  def index
    @posts = Post.all.includes(:post_category)
  end

  def edit;
  end

  def update
    @post.update_attributes(post_params)
    redirect_back(fallback_location: administrator_root_path)
  end

  def destroy
    @post.destroy
    redirect_back(fallback_location: administrator_root_path)
  end

  def new
    @post = Post.new
  end

  def create
    post = Post.new
    post.assign_attributes(post_params)
    if post.save
      redirect_to edit_administrators_post_path(post)
    else
      redirect_back(fallback_location: administrator_root_path)
    end
  end

  def show

  end

  private

  def get_post
    @post = Post.find(params[:id])
    @statistic_records = StatisticRecord.where(item_type: 0, item_id: @post.id)
  end

  def get_post_categories
    @post_categories = PostCategory.all.collect { |category| [category.title, category.id] }
  end

  def post_params
    params.require(:post).permit(:title, :description,  :avatar, :slug, :slug_en,
                                 :meta_title, :meta_description, :meta_keywords, :lead,
                                 :post_category_id)
  end
end
