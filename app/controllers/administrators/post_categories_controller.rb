class Administrators::PostCategoriesController < AdministratorsController

  before_action :get_post_category, only: [:edit, :update, :destroy]

  def index
    @post_categories = PostCategory.all
  end

  def edit; end

  def update
    @post_category.update_attributes(post_category_params)
    redirect_back(fallback_location: administrator_root_path)
  end

  def destroy
    if PostCategory.all.size > 1
      category = PostCategory.where.not(id: @post_category.id).first
      @post_category.posts.update_all(post_category_id: category.id)
      @post_category.destroy
    end
    redirect_back(fallback_location: administrator_root_path)
  end

  def new
    @post_category = PostCategory.new
  end

  def create
    post_category = PostCategory.new
    post_category.assign_attributes(post_category_params)
    if post_category.save
      redirect_to edit_administrators_post_category_path(post_category)
    else
      redirect_back(fallback_location: administrator_root_path)
    end
  end

  def show

  end

  private

  def get_post_category
    @post_category = PostCategory.find(params[:id])
  end

  def post_category_params
    params.require(:post_category).permit(:title,  :description,  :avatar, :large_avatar, :slug,
                                          :meta_title, :meta_description, :meta_keywords)
  end
end