class Publishers::PostsController < PublishersController

  before_action :get_post, only: [:edit, :update, :destroy, :publish, :unpublish]
  before_action :get_post_categories, only: [:edit, :new]

  def index
    Pblshr::PostPolicy.new(current_publisher, nil).index?
    @posts = current_publisher.posts.where(state: [:published, :not_published])
                                    .includes(:post_category).order('created_at DESC')
                              .limit(30)
  end

  def edit
    Pblshr::PostPolicy.new(current_publisher, @post).update?
  end

  def update
    Pblshr::PostPolicy.new(current_publisher, @post).update?
    @post.assign_attributes(post_params)
    @post.save ? flash[:success] = "Запись обновлена" : flash[:errors] = @post.errors.full_messages
    redirect_back(fallback_location: publisher_root_path)
  end

  def destroy
    Pblshr::PostPolicy.new(current_publisher, @post).destroy?
    @post.deleted! if @post.not_published?
    redirect_back(fallback_location: publisher_root_path)
  end

  def new
    Pblshr::PostPolicy.new(current_publisher, @post).create?
    @post = Post.new()
  end

  def create
    Pblshr::PostPolicy.new(current_publisher, @post).create?
    post = Post.new
    post.assign_attributes(post_params)
    post.assign_attributes({region_id: current_publisher.region_id, city_id: current_publisher.city_id})
    post.post_category_id = PostCategory.last.id if post.post_category_id == nil || post.post_category_id == 0
    post.fake_modificator = 0
    if post.save
      flash[:success] = "Запись создана"
      redirect_to edit_publishers_post_path(post)
    else
      flash[:errors] = post.errors.full_messages
      redirect_back(fallback_location: publisher_root_path)
    end
  end

  def publish
    Pblshr::PostPolicy.new(current_publisher, @post).update?
    @post.published! if @post.not_published?
    redirect_back(fallback_location: publisher_root_path)
  end

  def unpublish
    Pblshr::PostPolicy.new(current_publisher, @post).update?
    @post.not_published! if @post.published?
    redirect_back(fallback_location: publisher_root_path)
  end

  private

  def get_post
    @post = Post.find(params[:id])
  end

  def get_post_categories
    @post_categories = PostCategory.all.collect do |category| [category.title,
                                                               category.id]
    end
  end

  def post_params
    prm = params.require(:post).permit(:title, :description, :avatar, :lead, :post_category_id)
    prm['publisher_id'] = current_publisher.id
    prm['post_category_id'] = prm['post_category_id'].to_i
    prm
  end
end
