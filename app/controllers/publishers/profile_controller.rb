class Publishers::ProfileController < PublishersController

  def edit_profile
  end

  def update_password
    prm = params.require(:publisher).permit(:password, :password_confirmation)
    if current_publisher.update(prm)
      bypass_sign_in(current_publisher)
      flash[:success] = "Пароль обновлен"
    else
      flash[:errors] = ["Произошла ошибка при обновлении пароля"]
    end
    redirect_back(fallback_location: '/')
  end

  def update_profile
    if params[:publisher][:avatar]
      current_publisher.avatar = params[:publisher][:avatar]
      if current_publisher.save
        flash[:success] = "Профиль обновлен"
      else
        flash[:errors] = ["Произошла ошибка при загрузке изображения"]
      end
    else
      flash[:errors] = ["Произошла ошибка при обновлении профиля"]
    end
    redirect_back(fallback_location: '/')
  end

end