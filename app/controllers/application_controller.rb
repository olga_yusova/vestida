class ApplicationController < ActionController::Base
  include UiHelper
  include PostHelper

  skip_before_action :verify_authenticity_token, if: -> { controller_name == 'sessions' && action_name == 'create' }
end
