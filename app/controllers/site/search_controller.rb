class Site::SearchController < SiteController
  def show
    @posts = Post.search_everywhere(params[:s]).includes(:post_category).paginate(page: params[:page], per_page: 8)
    render 'site2/search'
  end
end
