class Site::RegionsController < SiteController

  def show
    @region = Region.find(params[:id])
  end

end