class Site::PostsController < SiteController
  def show
    post = Post.find_by(id: params[:id])
    post.total_views += 1
    post.save
    @post = Site::PostFacade.new(post)
    Statistic::Update.new(0, @post.base_object.id).call
    render "site2/post2"
  end
end
