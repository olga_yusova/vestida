class Site::CitiesController < SiteController

  before_action :get_city

  def show
    @city = Site::CityFacade.new(@city)
    @posts = Post.where(city_id: @city.base_object.id)
    render 'site2/city'
  end

  def post_category
    @post_category = Site::PostCategoryFacade.new(PostCategory.find(params[:post_category_id]), @city)

    @posts = Post.where(city_id: @city.id, post_category_id: @post_category.base_object.id).includes(:post_category, :publisher).paginate(page: params[:page], per_page: 8)
    @advert = Advert.get(@city.id)

    render "site2/city_post_category"
  end

  def feed
    @posts = Post.where(city_id: @city.id).order('created_at DESC')
                 .includes(:city, :region, :publisher, :post_category).where.not(description: nil)
                 .limit(1000)
    @settings = {
      title: "#{@city.title_ru} - актуальные новости",
      description: "Официальное сетевое издание. На этой странице представлены самые значимые, актуальные и важные новости города #{@city.title_ru}",
      link: city_url(@city)
    }
    render "site/pages/feeds", layout: false
  end

  private

  def get_city
    @city = City.friendly.find(params[:id])
  end
end
