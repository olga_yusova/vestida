class Site::PagesController < SiteController

  # главная страница
  def home
    @page = Site::HomePageFacade.new(Page.find_by(descriptor: 'home'), get_current_ip)
    @advert = Advert.visible.where(show_on_all_pages: true).order('RANDOM()').first
    render 'site2/home'
  end

  def show
    @page = Page.friendly.find(params[:id])
    render "site2/page"
  end

  def sitemap
    respond_to do |format|
      format.xml { render file: 'public/sitemaps/sitemap.xml' }
      format.html { redirect_to root_url }
    end
  end

  def robots
    @posts = Post.unscoped.where(state: :deleted)
    render "site/pages/robots.text.erb", layout: false, content_type: 'text/plain'
  end

  def feeds
    @settings = {
      title: 'VestiDa - актуальные новости',
      description: 'На сайте Vestida.ru всегда свежие новости за день и неделю.',
      link: 'https://vestida.ru/'
    }
    @posts = Post.all.order('created_at DESC')
                 .includes(:city, :region, :publisher, :post_category).where.not(description: nil)
                 .limit(1000)
  end

  def feeds_hot
    @settings = {
      title: 'VestiDa - популярные новости',
      description: 'Самые популярные новости за неделю',
      link: 'https://vestida.ru/'
    }
    @posts = Post.all.order('RANDOM()')
                 .includes(:city, :region, :publisher, :post_category).where.not(description: nil)
                 .limit(1000)
    render 'site/pages/feeds', layout: false
  end

  private

  def get_current_ip
    if Rails.env.development?
      '77.88.5.5'
    else
      request.remote_ip
    end
  end
end
