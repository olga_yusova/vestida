class Site::PostCategoriesController < SiteController

  def show
    @category = PostCategory.find(params[:id])
    @category = Site::PostCategoryFacade.new(@category)
  end
end