class Site::CommentsController < SiteController
  def create
    post = Post.find_by(id: params[:post_comment][:post_id])
    if current_user && post
      post.post_comments.create(content: params[:post_comment][:content], user_id: current_user.id)
    end
    redirect_back(fallback_location: user_root_path)
  end

  def destroy
    comment = PostComment.find_by(id: params[:id])
    if comment && (current_administrator || (current_user && comment.user_id == current_user.id))
      comment.update_attribute(:deleted, :true)
    end
    redirect_back(fallback_location: user_root_path)
  end
end