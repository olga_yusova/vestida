class Site::ApiController < SiteController
  skip_before_action :verify_authenticity_token

# получение городов у региона
  def region_cities
    region = Region.find(params[:region_id]) rescue nil
    if region
      render json: region.cities.order(:title_ru).collect{|city| {title_ru: city.title_ru, url: city_path(city), id: city.id} }
      # render json:  region.cities.select(:id, :title_ru)
    else
      render json: {status: 400}
    end
  end

end
