class Site::AdvertsController < SiteController
  def show
    advert = Advert.friendly.find(params[:id])
    advert.total_redirects += 1
    advert.save
    redirect_to "http://#{advert.url}"
  end

  def info
    @advert = Advert.friendly.find(params[:id])
    render "site/adverts/info", layout: false
  end
end
