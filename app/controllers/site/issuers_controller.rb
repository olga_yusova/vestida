class Site::IssuersController < SiteController
  # контроллер издателей
  # переименовал вместо PublishersController чтобы не было коллизий в роутах
  def show
    @publisher = Site::PublisherFacade.new(Publisher.find(params[:id]))
    @posts = Post.where(publisher_id: @publisher.base_object.id).order('created_at DESC').includes(:post_category).paginate(page: params[:page], per_page: 8)
    render 'site2/publisher'
  end
end
