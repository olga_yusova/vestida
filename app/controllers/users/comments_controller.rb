class Users::CommentsController < UsersController

  def index
    @comments = PostComment.where(user_id: current_user.id).includes(:post)
  end

  def destroy
    PostComment.find(params[:id]).destroy
    redirect_back(fallback_location: user_root_path)
  end
end