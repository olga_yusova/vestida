class Users::PostsController < UsersController

  before_action :get_post, only: [:edit, :update, :destroy]
  before_action :get_post_categories, only: [:edit, :new]

  def index
    @posts = current_user.posts.all.includes(:post_category)
  end

  def edit;
  end

  def update
    @post.update_attributes(post_params)
    redirect_back(fallback_location: user_root_path)
  end

  def destroy
    @post.destroy
    redirect_back(fallback_location: user_root_path)
  end

  def new
    @post = Post.new
  end

  def create
    post = Post.new(user_id: current_user.id)
    post.assign_attributes(post_params)
    if post.save
      redirect_to edit_users_post_path(post)
    else
      redirect_back(fallback_location: user_root_path)
    end
  end

  def show

  end

  private

  def get_post
    @post = Post.find(params[:id])
    @post.user_id == current_user.id ? @post : nil
  end

  def get_post_categories
    @post_categories = PostCategory.all.collect { |category| [category.title, category.id] }
  end

  def post_params
    params.require(:post).permit(:title, :description,  :avatar, :large_avatar, :slug, :slug_en,
                                 :meta_title, :meta_description, :meta_keywords, :lead,
                                 :post_category_id)
  end
end