class UsersController < ApplicationController
  before_action :authenticate_user! # это нужно для девайса

  layout "user"

  def after_sign_in_path_for(resource)
    administrator_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  def dashboard
    redirect_to '/'
  end
end