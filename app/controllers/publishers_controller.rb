class PublishersController < ApplicationController
  before_action :authenticate_publisher!

  layout "publisher"

  def after_sign_in_path_for(resource)
    publisher_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  def dashboard
    render 'publishers/dashboard'
  end
end