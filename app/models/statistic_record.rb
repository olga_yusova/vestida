class StatisticRecord < ApplicationRecord

  def fake_views
    if self.date == DateTime.now.to_date
      real_views
    else
      real_views + 1000
    end
  end

  def real_views
    self.views.inject(:+)
  end
end
