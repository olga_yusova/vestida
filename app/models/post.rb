class Post < ApplicationRecord
  include PgSearch

  before_create :add_basic_data

  mount_uploader :avatar,       PostAvatarUploader

  has_many :post_comments
  belongs_to :post_category, optional: true
  belongs_to :publisher, optional: true
  belongs_to :region
  belongs_to :city

  enum state: { deleted: 0, not_published: 1, published: 2 }

  validates :title, presence: true, length: {maximum: 400}
  validates :lead, length: {maximum: 400}
  # validates :description, length: {maximum: 500000}

  default_scope { where.not(state: :deleted) }

  pg_search_scope :search_everywhere, against: [:title, :description, ]

  def public_total_views
    self.fake_modificator + self.total_views
  end

  private

  def add_basic_data
    publisher = self.publisher
    self.city_id = publisher.city_id
    self.region_id = publisher.region_id
  end
end
