class PostComment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  default_scope { where(deleted: false) }

  validates :content, presence: true, length: 5..2000
end
