class Region < ApplicationRecord

  has_many :cities
  has_many :publishers
  has_many :posts
end