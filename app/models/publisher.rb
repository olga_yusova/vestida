class Publisher < ApplicationRecord
  extend FriendlyId
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  mount_uploader :avatar,       PublisherAvatarUploader

  has_many :posts
  has_many :adverts
  belongs_to :city
  belongs_to :region

  default_scope { where(deleted: false) }

  scope :deleted_all, -> { unscope(:where).where deleted: true }

  def info
    "Краткая информация о издателе"
  end

  friendly_id :slug, use: :slugged
end
