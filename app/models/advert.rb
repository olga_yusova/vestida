class Advert < ApplicationRecord

  extend FriendlyId
  friendly_id :slug, use: :slugged

  mount_uploader :image, AdvertAvatarUploader
  belongs_to :city

  scope :visible, -> { where(deleted: false) }


  def self.get(city_id)
    advert = get_advert(city_id)
    if advert
      advert.total_views += 1
      advert.save
    end
    advert
  end

  private

  def self.get_advert(city_id)
    arr = Advert.visible.where(city_id: city_id).pluck(:id)
    Advert.visible.where(show_on_all_pages: true).pluck(:id).each {|id| arr << id}
    Advert.visible.find_by(id: arr.sample)
  end
end
