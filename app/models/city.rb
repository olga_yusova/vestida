class City < ApplicationRecord
  extend FriendlyId

  has_many :publishers
  has_many :posts
  has_many :adverts
  belongs_to :region
  
  friendly_id :slug, use: :slugged
end
