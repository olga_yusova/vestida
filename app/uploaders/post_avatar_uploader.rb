class PostAvatarUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick
  include ContentImage
  require 'carrierwave/storage/fog'

  storage IMAGE_STORAGE

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :admin_edit do
    process resize_to_fill: [203,130]
  end

  # version :carousel_element do
  #   process resize_to_fill: [300, 168]
  # end
  #
  # version :list_element do
  #   process resize_to_fill: [260, 146]
  # end
  #
  # version :mini_list_element do
  #   process resize_to_fill: [80, 80]
  # end
  #
  # version :related_posts do
  #   process resize_to_fill: [266, 260]
  # end
  #
  # version :main_page_carousel do
  #   process resize_to_fill: [295, 400]
  # end

  version :s150_150 do
    process resize_to_fill: [150, 150]
    process :convert => 'jpg'
  end

  version :s250_250 do
    process resize_to_fill: [250, 250]
    process :convert => 'jpg'
  end

  version :s261_182 do
    process resize_to_fill: [261, 182]
    process :convert => 'jpg'
  end

  version :s87_87 do
    process resize_to_fill: [87, 87]
    process :convert => 'jpg'
  end

  version :s80_80 do
    process resize_to_fill: [80, 80]
    process :convert => 'jpg'
  end
end
