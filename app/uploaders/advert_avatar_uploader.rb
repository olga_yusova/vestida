class AdvertAvatarUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick
  include ContentImage
  require 'carrierwave/storage/fog'

  storage IMAGE_STORAGE

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :admin_edit do
    process resize_to_fill: [203,130]
  end

  version :s338_190 do
    process resize_to_fill: [338, 190]
    process :convert => 'jpg'
  end
end
