class PublisherAvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include ContentImage
  require 'carrierwave/storage/fog'

  storage IMAGE_STORAGE

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :admin_edit do
    process resize_to_fill: [203,130]
  end

  # version :list_element do
  #   process resize_to_fill: [260, 146]
  # end

  version :s150_150 do
    process resize_to_fill: [150, 150]
    process :convert => 'jpg'
  end

  version :s90_90 do
    process resize_to_fill: [90, 90]
    process :convert => 'jpg'
  end

  version :s250_250 do
    process resize_to_fill: [250, 250]
    process :convert => 'jpg'
  end

  version :s261_182 do
    process resize_to_fill: [250, 250]
    process :convert => 'jpg'
  end

  version :s250_120 do
    process resize_to_fill: [250, 120]
    process :convert => 'jpg'
  end
end
