module ContentImage
  include ActiveSupport::Concern

  if Rails.env.production?
    IMAGE_STORAGE = :file
  else
    IMAGE_STORAGE = :file
  end

end